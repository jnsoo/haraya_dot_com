FactoryBot.define do
  factory :classroom do
    name { 'Classroom Name' }
    description { 'Classroom Description' }
    looking_for { 'Classroom Looking For' }
    user
    public { false }
    sequence(:group_id) { |n| "test#{n}" }
  end
end
