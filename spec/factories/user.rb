FactoryBot.define do
  factory :user do
    name { 'Name' }
    age { 18 }
    password { 'password' }
    password_confirmation { 'password' }
    sequence(:email) { |n| "user#{n}@example.com" }
    sequence(:username) { |n| "user#{n}" }
  end
end
