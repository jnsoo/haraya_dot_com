FactoryBot.define do
  factory :notification do
    user
    message { 'message' }
    is_read { false }
    link { '/posts/1' }
    connected_id { 'post:1' }
  end
end
