require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { build :user, username: 'username123' }

  it 'has a valid factory' do
    expect(user).to be_valid
  end

  describe 'Class Methods' do
    let(:fake_info) { double('FakeInfo', email: 'fake_email@example.com') }
    let(:fake_credentials) do
      double('FakeCredential', 
             token: 'token', 
             expires: true, 
             expires_at: Date.today.to_time.to_i,
             refresh_token: 'refresh_token')
    end
    let(:fake_auth) do
      double('FakeAuth', 
             provider: 'google_oauth2', 
             uid: '123', 
             info: fake_info,
             credentials: fake_credentials)
    end

    describe '#from_omniauth' do
      it 'creates a user when provider and uid isnt found' do
        expect { User.from_omniauth(fake_auth) }
          .to change { User.count }.by(1)

        user_last = User.last

        expect(user_last.provider).to eq 'google_oauth2'
        expect(user_last.uid).to eq '123'
        expect(user_last.email).to eq 'fake_email@example.com'
        expect(user_last.token).to eq 'token'
        expect(user_last.expires).to be_truthy
        expect(user_last.expires_at).to eq Date.today.to_time.to_i
        expect(user_last.refresh_token).to eq 'refresh_token'
      end
    end

    context 'when user with provider and uid exists' do
      let!(:user) { create :user, provider: 'google_oauth2', uid: '123' }
      it { expect(User.from_omniauth(fake_auth)).to eq user }
    end
  end

  describe 'Instance Methods' do
    describe '#to_param' do
      it { expect(user.to_param).to eq 'username123' }
    end

    describe '#unread_notifications' do
      let!(:notif_1) { create :notification, user: user, is_read: false }
      let!(:notif_2) { create :notification, user: user, is_read: false }

      it { expect(user.unread_notifications).to eq [notif_1, notif_2] }
    end

    describe '#incomplete_credentials?' do
      it { expect(user.incomplete_credentials?).to be_falsy }

      context 'when user has blank attributes' do
        let(:user) do
          build :user,
            name: nil,
            age: nil,
            username: nil,
            encrypted_password: ''
        end

        it { expect(user.incomplete_credentials?).to be_truthy }
      end
    end

    describe '#has_class?' do
      let(:classroom) { create :classroom }
      let(:classroom_2) { create :classroom }
      let(:user) { create :user }
      let!(:uc) { create :user_classroom, user: user, classroom: classroom }

      it { expect(user.has_class?(classroom)).to be_truthy }
      it { expect(user.has_class?(classroom_2)).to be_falsy }
    end
  end
end
