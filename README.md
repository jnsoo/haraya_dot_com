# Installation

### Run the following:
- `cd path/to/dir`
- `git clone https://github.com/jnsoo123/haraya_dot_com.git`
  - enter your github username and password
- `cd haraya_dot_com`
- create a file `.env` in root of the file and put this in it
```
GOOGLE_CLIENT_ID=209087269057-qego62i02ejiijjva4qteo66d5bpnl73.apps.googleusercontent.com
GOOGLE_CLIENT_SECRET=B8PVkdaGeSS2Dx6zR67j2S3i
```
  - this is the google api credentials needed for you to login in your local
- `gem install bundler`
- `bundle install`
- `rails db:create db:mgirate`
  - make sure your mysql is running

To run the app `rails server`. 

Enter `localhost:3000` in your browser
