class Skills extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      editMode: false,
      skillName: '',
      skills: this.props.skills
    }
  }

  toggleEditMode() {
    var editMode = this.state.editMode
    this.setState({ editMode: !editMode })
  }

  updateSkillName(event) {
    this.setState({ skillName: event.target.value })
  }

  deleteSkill(id, event) {
    event.preventDefault()

    $.ajax({
      url: `/skills/${id}`,
      method: 'DELETE',
      data: {
        authenticity_token: $('meta[name="csrf-token"]').attr('content')
      },
      success: (response) => {
        this.setState({ skills: response[this.props.kind_of] })
      },
      error: (response) => {
        alert(response.resnponseJSON.error_message)
      }
    })
  }

  createSkill(event) {
    event.preventDefault()

    $.ajax({
      url: '/skills',
      method: 'POST',
      data: { 
        skill: { name: this.state.skillName, kind_of: this.props.kind_of },
        authenticity_token: $('meta[name="csrf-token"]').attr('content')
      },
      success: (response) => {
        this.setState({ skills: response[this.props.kind_of] })         
      },
      error: (response) => {
        alert(response.responseJSON.error_message)
      },
      complete: () => { this.setState({ editMode: false, skillName: '' }) }
    })
  }

  textboxPlaceholder() {
    if (this.props.kind_of == 'already_know') {
      return 'Enter a skill that you already know'
    } else if (this.props.kind_of == 'want_to_learn') {
      return 'Enter a skill that you want to learn'
    } else {
      return 'Enter a skill that you are currently learning'
    }
  }

  buttonClass() {
    if (this.props.kind_of == 'already_know') {
      return 'btn-success'
    } else if (this.props.kind_of == 'want_to_learn') {
      return 'btn-warning'
    } else {
      return 'btn-info'
    }
  }

  renderTextbox() {
    if (this.state.editMode) {
      return(
        <div className='form-group' style={{ marginTop: '20px' }}>
          <form onSubmit={this.createSkill.bind(this)}>
            <input 
              className="form-control form-control-sm" 
              type="text" 
              placeholder={this.textboxPlaceholder()} 
              onChange={this.updateSkillName.bind(this)}
              value={this.state.skillName} />
          </form>
        </div>
      )
    }
  }
  
  renderSkills() {
    return(this.state.skills.map((skill, index) => {
      return(<div className='dropdown' key={index}>
        <button 
          key={index} 
          type='button' 
          id={`skill_id_${skill.id}`} 
          data-toggle={this.props.controller != 'show' ? 'dropdown' : ''}
          data-offset='-30,0'
          className={`btn btn-sm skills ${this.buttonClass()}`}>
        {skill.name}
        </button>
        <div className='dropdown-menu' style={{ maxWidth: '20px' }}>
          <a 
            className='dropdown-item' 
            onClick={this.deleteSkill.bind(this, skill.id)}
            href='#'>
            Delete
          </a>
        </div>
      </div>)
    }))
  }

  renderAddMoreSkills() {
    if (this.props.controller == 'show') return
    return(
      <button 
        className={`btn btn-sm ${this.buttonClass()}`} 
        onClick={this.toggleEditMode.bind(this)}>
        add more +
      </button>
    )
  }

  render() {
    return(
      <div>
        <div className='btn-group'>
          {this.renderSkills()}
        </div>
        {this.renderAddMoreSkills()}
        {this.renderTextbox()}
      </div>
    )
  }
}
