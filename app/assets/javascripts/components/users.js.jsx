class Users extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      users: this.props.users,
      requests: this.props.requests
    }
  }

  disconnect(user, event) {
    event.preventDefault()
    console.log(user)

    $.ajax({
      url: `/friendships/${user.id}?${window.location.search.substr(1)}`,
      method: 'DELETE',
      data: { authenticity_token: $('meta[name="csrf-token"]').attr('content') },
      success: (response) => {
        alert('Connection Removed')
        this.setState({ requests: response.requests, users: response.users })
      }
    })
  }

  connect(user, event) {
    event.preventDefault()
    console.log(user)

    $.ajax({
      url: `/friendships?${window.location.search.substr(1)}`,
      method: 'POST',
      data: {
        id: user.id,
        authenticity_token: $('meta[name="csrf-token"]').attr('content')
      },
      success: (response) => {
        alert('Connection Requested!')
        this.setState({ requests: response.requests, users: response.users })
      }
    })
  }

  acceptRequest(user, accepted, event) {
    event.preventDefault()

    $.ajax({
      url: `/friendships/${user.id}?${window.location.search.substr(1)}`,
      method: 'PUT',
      data: {
        accepted: accepted,
        authenticity_token: $('meta[name="csrf-token"]').attr('content')
      },
      success: (response) => {
        alert(`Connection ${accepted ? 'accepted' : 'declined'}`)
        this.setState({ requests: response.requests, users: response.users })
      }
    })
  }

  renderConnectButtons(user) {
    if (user.connected) {
      return <a href='#'
        onClick={this.disconnect.bind(this, user)}
        className='btn btn-danger btn-sm'>
        Remove Connection
      </a>
    } else if (user.pending) {
      return <a href='#'
        className='btn btn-secondary btn-sm disabled'>
        Request Pending
      </a>
    } else {
      return <a href='#'
        onClick={this.connect.bind(this, user)}
        className='btn btn-primary btn=sm'>
        Connect
      </a>
    }
  }

  renderRequests() {
    if (this.state.requests.length == 0)
      return 'No Requests'

    return(this.state.requests.map((user, index) => {
      return(<div key={index} className='card'>
        <div className='card-body'>
          <div className='float-left'>
            {user.name}
          </div>
          <div className='float-right'>
            <a href='#'
              className='btn btn-link btn-sm'
              onClick={this.acceptRequest.bind(this, user, false)}>
              Decline Connection
            </a>
          </div>
          <div className='float-right'>
            <a href='#'
              className='btn btn-primary btn-sm'
              onClick={this.acceptRequest.bind(this, user, true)}>
              Accept Connection
            </a>
          </div>
        </div>
        </div>)
    }))
  }

  render() {
    return(<div>
      {this.state.users.map((user, index) => {
        return(<div key={index} className='card'>
          <div className='card-body'>
            <div className='float-left'>
              <a href={`/users/${user.username}`}>{user.name}</a>
            </div>
            <div className='float-right'>
              {this.renderConnectButtons(user)}
            </div>
          </div>
        </div>)
      })}

      <br />

      <h1>Connection Requests</h1>
      <hr />

      {this.renderRequests()}
    </div>)
  }
}
