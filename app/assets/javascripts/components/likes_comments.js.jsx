class LikesComments extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      like_count: this.props.like_count,
      liked: this.props.liked,
      showCommentBox: false,
      comment: '',
      comments: this.props.comments
    }
  }

  addLike(e) {
    e.preventDefault()

    $.ajax({
      url: '/likes',
      method: 'POST',
      data: {
        authenticity_token: $('meta[name="csrf-token"]').attr('content'),
        post_id: this.props.post_id
      },
      success: (response) => {
        this.setState({ like_count: response.like_count, liked: response.liked })
      }
    })
  }

  addComment(e) {
    e.preventDefault()

    var showCommentBox = this.state.showCommentBox
    this.setState({ showCommentBox: !showCommentBox })
  }

  submitComment(e) {
    e.preventDefault()
    console.log(this.state.comment)

    $.ajax({
      url: '/comments',
      method: 'POST',
      data: {
        authenticity_token: $('meta[name="csrf-token"]').attr('content'),
        comment: {
          post_id: this.props.post_id,
          body: this.state.comment
        }
      },
      success: (response) => {
        this.setState({ comments: response.comments, comment: '', showCommentBox: false })
      }
    })
  }

  updateComment(e) {
    this.setState({ comment: e.target.value })
  }

  renderLikeCount() {
    if (this.state.like_count != 0) { return this.state.like_count }
  }

  renderCommentForm() {
    if (this.state.showCommentBox) {
      return(<div className='mb-2'>
        <form action='#' onSubmit={this.submitComment.bind(this)}>
          <input
            type='text'
            required
            className='form-control'
            placeholder='Write a comment'
            onChange={this.updateComment.bind(this)}
            value={this.state.comment} />
        </form>
      </div>)
    }
  }

  renderComments() {
    return(this.state.comments.map((comment, index) => {
      return(
        <div className='comment-box' key={index}>
          <a href={`/users/${comment.user.username}`}>{comment.user.name}</a> {comment.body}
        </div>
      )
    }))
  }

  likeButtonClass() {
    if (this.state.liked) {
      return 'btn-primary'
    } else {
      return 'btn-secondary'
    }
  }

  render() {
    return(
      <div>
        <a
          className={`btn ${this.props.disabled ? 'disabled' : ''} btn-sm mr-2 ${this.likeButtonClass()}`}
          onClick={this.addLike.bind(this)}
          href='#'>
          <i className='fas fa-thumbs-up'></i> {this.renderLikeCount()} Understood
        </a>
        <a
          className={`btn ${this.props.disabled ? 'disabled' : ''} btn-secondary btn-sm`}
          onClick={this.addComment.bind(this)}
          href='#'>
          <i className='far fa-comment'></i> Comment
        </a>
        <hr />
        {this.renderCommentForm()}
        {this.renderComments()}
      </div>
    )
  }
}
