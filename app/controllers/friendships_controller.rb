class FriendshipsController < ApplicationController
  def create
    current_user.friend_request(connection)
    render json: { users: fetched_users, requests: fetched_requests }, status: 200
  end

  def update
    if request_accepted?
      current_user.accept_request(connection)
    else
      current_user.decline_request(connection)
    end

    render json: { users: fetched_users, requests: fetched_requests }, status: 200
  end

  def destroy
    current_user.remove_friend(connection)
    render json: { users: fetched_users, requests: fetched_requests }, status: 200
  end

  private

  def connection
    User.find(params[:id])
  end

  def request_accepted?
    params[:accepted] == 'true'
  end
end
