class Classrooms::PostsController < ApplicationController
  before_action :set_classroom
  prepend_before_action :clear_notifications, only: :show

  def new
    @post = @classroom.posts.new
  end

  def edit
    @post = @classroom.posts.find(params[:id])
  end

  def show
    redirect_to classrooms_path, notice: "Please join #{@classroom.name} first." unless show_class?

    @user_like_ids = current_user.likes.pluck(:post_id)
    @post = @classroom.posts.find(params[:id])
    @comments = Hash.new
    @renderer = Redcarpet::Markdown.new(Redcarpet::Render::HTML)
    @comments[@post.id] = @post.comments.collect do |comment|
      {
        body: comment.body,
        user: comment.user
      }
    end
  end

  def create
    @post = @classroom.posts.new(post_params)
    @post.user = current_user

    if @post.save
      redirect_to classroom_path(@classroom), notice: 'Post created !!'
    end
  end

  def update
    @post = @classroom.posts.find(params[:id])

    if @post.update(post_params)
      redirect_to classroom_post_path(@post, classroom_id: @classroom.id), notice: 'Post updated !!'
    end
  end

  def destroy
    @post = @classroom.posts.find(params[:id])

    if @post.destroy
      redirect_to classroom_path(@classroom), alert: 'Post deleted !!'
    end
  end

  private

  def set_classroom
    @classroom = Classroom.find(params[:classroom_id])
  end

  def post_params
    params.require(:post).permit(:body)
  end

  def clear_notifications
    Notification.where(
      user_id: current_user.id,
      connected_id: "post:#{params[:id]}",
      is_read: false
    ).update_all(is_read: true)
  end

  def show_class?
    current_user.has_class?(@classroom) || @classroom.public?
  end
end
