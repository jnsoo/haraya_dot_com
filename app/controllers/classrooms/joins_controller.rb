class Classrooms::JoinsController < ApplicationController
  def create
    @classroom = Classroom.find(params[:id])
    current_user.classrooms << @classroom
    redirect_to my_classes_path, notice: "Class #{@classroom.name} joined !!"
  end

  def destroy
    classroom = Classroom.find(params[:id])

    if UserClassroom.find_by_classroom_id(classroom.id).try(:destroy)
      redirect_to my_classes_path, alert: "Left class #{classroom.name}"
    end
  end
end
