class Classrooms::MyClassesController < ApplicationController
  def index
    @q = current_user.classrooms.ransack(params[:q])
    @classrooms = @q.result
  end
end
