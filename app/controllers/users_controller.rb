class UsersController < ApplicationController
  def index
    @users = fetched_users
    @requests = fetched_requests
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user

    if @user.update(user_params)
      redirect_to profiles_path, notice: 'User profile updated !!'
    else
      render :edit
    end
  end

  def show
    @user = User.find_by_username(params[:id])
    
    if @user == current_user
      redirect_to profiles_path
      return
    end

    if user_not_connected? && !@user.public?
      redirect_to users_path, alert: 'Please connect to that user first'
      return
    end

    @already_know_skills = user_skills(@user)[:already_know]
    @currently_learning_skills = user_skills(@user)[:currently_learning]
    @want_to_learn_skills = user_skills(@user)[:want_to_learn]

    @posts = Post.includes(:comments).where(classroom_id: nil, user: @user).order(id: :desc)
    @user_like_ids = current_user.likes.pluck(:post_id)
    @renderer = Redcarpet::Markdown.new(Redcarpet::Render::HTML)
    @comments = Hash.new

    @posts.each do |post|
      @comments[post.id] = post.comments.collect do |comment|
        {
          body: comment.body,
          user: comment.user
        }
      end
    end
  end

  private

  def user_not_connected?
    !current_user.friends_with?(@user)
  end

  def user_params
    params.require(:user).permit(:name, :age, :public, :username)
  end
end
