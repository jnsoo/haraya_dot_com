class CommentsController < ApplicationController
  def create
    comment = Comment.new(comment_params)
    comment.user = current_user

    if comment.save
      render json: { comments: comments }, status: 200
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body, :post_id)
  end

  def comments
    post = Post.find(params[:comment][:post_id])
    post.comments.collect do |comment|
      {
        body: comment.body,
        user: comment.user.as_json
      }
    end
  end
end
