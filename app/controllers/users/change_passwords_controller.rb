class Users::ChangePasswordsController < ApplicationController
  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.valid_password?(params[:user][:old_password]) && password_confirmed?
      @user.update(user_params)
      redirect_to profiles_path, notice: 'Password updated !!'
    else
      flash[:alert] = 'Invalid Password'
      render :edit
    end
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def password_confirmed?
    params[:user][:password] == params[:user][:password_confirmation]
  end
end
