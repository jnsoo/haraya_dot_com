class Users::RegistrationsController < ApplicationController
  skip_before_action :check_user_credentials

  def show
    @user = current_user
  end

  def update
    @user = current_user

    if password_confirmed?
      if @user.update(user_params)
        bypass_sign_in current_user
        redirect_to root_path, notice: 'User profile updated !!'
      else
        render :show
      end
    else
      flash[:alert] = 'Passwords does not match'
      render :show
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :age, :username, :password, :password_confirmation)
  end

  def password_confirmed?
    params[:user][:password] == params[:user][:password_confirmation]
  end
end
