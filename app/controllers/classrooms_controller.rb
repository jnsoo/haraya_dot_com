class ClassroomsController < ApplicationController
  def index
    classes_already_joined_ids = current_user.classrooms.pluck(:id)
    @q = Classroom.where.not(id: classes_already_joined_ids).ransack(params[:q])
    @classrooms = @q.result
  end

  def new
    @classroom = Classroom.new
  end

  def edit
    @classroom = Classroom.find_by_group_id(params[:id])

    if @classroom.user != current_user
      redirect_to classroom_path(@classroom), alert: 'You do not have permission for this action.'
      return
    end
  end

  def update
    @classroom = Classroom.find_by_group_id(params[:id])

    if @classroom.user != current_user
      redirect_to classroom_path(@classroom), alert: 'You do not have permission for this action.'
      return
    end

    if @classroom.update(classroom_params)
      redirect_to classroom_path(@classroom), notice: 'Classroom has been updated !!'
    else
      render :edit
    end
  end

  def show
    @classroom = Classroom.find_by_group_id(params[:id])
    redirect_to classrooms_path, notice: "Please join #{@classroom.name} first." unless show_class?

    @posts = @classroom.posts.order(id: :desc)
    @user_like_ids = current_user.likes.pluck(:post_id)
    @renderer = Redcarpet::Markdown.new(Redcarpet::Render::HTML)
    @comments = Hash.new

    @posts.each do |post|
      @comments[post.id] = post.comments.collect do |comment|
        {
          body: comment.body,
          user: comment.user
        }
      end
    end
  end

  def create
    @classroom = Classroom.new(classroom_params)
    @classroom.user = current_user

    if @classroom.save
      current_user.classrooms << @classroom
      redirect_to my_classes_path, notice: 'Class created !!'
    else
      render :new
    end
  end

  private

  def classroom_params
    params.require(:classroom).permit(:name, :description, :looking_for, :public, :group_id)
  end

  def show_class?
    current_user.has_class?(@classroom) || @classroom.public?
  end
end
