class SkillsController < ApplicationController
  def create
    @skill = Skill.new(skill_params)
    @skill.user = current_user

    if @skill.save
      render json: { 
        already_know:       user_skills[:already_know], 
        currently_learning: user_skills[:currently_learning],
        want_to_learn:      user_skills[:want_to_learn]
      }, status: 200
    else
      render json: { error_message: @skill.errors.full_messages }, status: 500
    end
  end

  def destroy
    @skill = Skill.find(params[:id])

    if @skill.destroy
      render json: { 
        already_know:       user_skills[:already_know], 
        currently_learning: user_skills[:currently_learning],
        want_to_learn:      user_skills[:want_to_learn]
      }, status: 200
    else
      render json: { error_message: @skill.errors.full_messages }, status: 500
    end
  end

  private

  def skill_params
    params.require(:skill).permit(:name, :kind_of)
  end
end
