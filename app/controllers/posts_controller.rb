class PostsController < ApplicationController
  before_action :set_post, only: [:edit, :update, :destroy]
  prepend_before_action :clear_notifications, only: :show

  def new
    @post = Post.new
  end

  def edit
  end

  def show
    @user_like_ids = current_user.likes.pluck(:post_id)
    @post = Post.find(params[:id])
    @comments = Hash.new
    @renderer = Redcarpet::Markdown.new(Redcarpet::Render::HTML)
    @comments[@post.id] = @post.comments.collect do |comment|
      {
        body: comment.body,
        user: comment.user
      }
    end
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user

    if @post.save
      redirect_to root_path, notice: 'Post created !!'
    else
      flash[:warning] = 'Post cannot be empty.'
      render :new
    end
  end

  def update
    if @post.update(post_params)
      redirect_to post_path(@post), notice: 'Post updated !!'
    end
  end

  def destroy
    if @post.destroy
      redirect_to root_path, alert: 'Post deleted !!'
    end
  end

  private

  def post_params
    params.require(:post).permit(:body)
  end

  def set_post
    @post = Post.find(params[:id])
  end

  def clear_notifications
    Notification.where(
      user_id: current_user.id,
      connected_id: "post:#{params[:id]}",
      is_read: false
    ).update_all(is_read: true)
  end
end
