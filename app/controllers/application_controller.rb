class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  before_action :check_user_credentials
  before_action :get_notifications
  before_action :configure_permitted_parameters, if: :devise_controller?

  layout :resource_by_name

  protected

  def get_notifications
    return if current_user.nil?
    @notifications = current_user.unread_notifications.last(10).reverse
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :age, :public, :email])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:email])
  end

  def check_user_credentials
    return if current_user.nil?
    redirect_to registration_path if current_user.incomplete_credentials?
  end

  def user_skills(user = current_user)
    already_know_skills, currently_learning_skills, want_to_learn_skills = [], [], []

    user.skills.collect do |skill|
      case skill.kind_of
      when 'already_know'
        already_know_skills << skill
      when 'currently_learning'
        currently_learning_skills << skill
      when 'want_to_learn'
        want_to_learn_skills << skill
      end
    end

    {
      already_know: already_know_skills,
      currently_learning: currently_learning_skills,
      want_to_learn: want_to_learn_skills
    }
  end

  def resource_by_name
    if devise_controller? || controller_name == 'registrations'
      'devise'
    else
      'application'
    end
  end

  def fetched_users
    @q = User.ransack(params[:q])
    return [] if params[:q].try(:[], :name_or_email_cont).blank?
    pending_friend_ids = current_user.pending_friends.pluck(:id)
    collected_users.collect do |user|
      {
        id: user.id,
        name: user.name,
        username: user.username,
        connected: current_user.friends_with?(user),
        pending: pending_friend_ids.include?(user.id)
      }
    end
  end

  def fetched_requests
    current_user.requested_friends
  end

  def collected_users
    users = @q.result(distinct: true).where.not(id: current_user.id)

    case params[:kind_of]
    when 'connections'
      ids = current_user.friends.pluck(:id)
      users.where(id: ids)
    when 'non_connections'
      ids = current_user.friends.pluck(:id)
      users.where.not(id: ids)
    else
      users
    end
  end
end
