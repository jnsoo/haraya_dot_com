class HomeController < ApplicationController
  def index
    @posts = Post.includes(:comments).where(classroom_id: nil).order(id: :desc)
    @user_like_ids = current_user.likes.pluck(:post_id)
    @renderer = Redcarpet::Markdown.new(Redcarpet::Render::HTML)
    @comments = Hash.new

    @posts.each do |post|
      @comments[post.id] = post.comments.collect do |comment|
        {
          body: comment.body,
          user: comment.user
        }
      end
    end
  end
end
