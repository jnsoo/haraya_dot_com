class LikesController < ApplicationController
  def create
    @post = Post.find(params[:post_id])

    if user_already_liked_the_post?
      @like.destroy
      @liked = false
    else
      Like.create(post: @post, user: current_user)
      @liked = true
    end

    render json: { like_count: @post.likes.count, liked: @liked }
  end

  private

  def user_already_liked_the_post?
    @like = Like.find_by_post_id_and_user_id(@post.id, current_user.id)
    @like.present?
  end
end
