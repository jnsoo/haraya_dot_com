class ProfilesController < ApplicationController
  def index
    @already_know_skills       = user_skills[:already_know]
    @currently_learning_skills = user_skills[:currently_learning]
    @want_to_learn_skills      = user_skills[:want_to_learn]

    @posts = current_user.posts.includes(:comments).where(classroom_id: nil).order(id: :desc)
    @renderer = Redcarpet::Markdown.new(Redcarpet::Render::HTML)
    @user_like_ids = current_user.likes.pluck(:post_id)

    @comments = Hash.new
    @posts.each do |post|
      @comments[post.id] = post.comments.collect do |comment|
        {
          body: comment.body,
          user: comment.user
        }
      end
    end
  end
end
