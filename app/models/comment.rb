class Comment < ApplicationRecord
  belongs_to :post
  belongs_to :user

  after_create :send_notification

  private

  def send_notification
    users_to_send_notifications_to.each do |user_id|
      next if self.user_id == user_id

      link = if self.post.classroom_id.present?
               Rails.application.routes.url_helpers.classroom_post_path(self.post, classroom_id: self.post.classroom_id)
             else
               Rails.application.routes.url_helpers.post_path(self.post)
             end

      message = if self.post.user_id == user_id
                  "#{self.user.name} has commented \"#{self.body.first(20)}...\" on your post."
                else
                  "#{self.user.name} has commented \"#{self.body.first(20)}...\" on the post you commented on."
                end

      Notification.create do |notif|
        notif.user_id      = user_id
        notif.message      = message
        notif.link         = link
        notif.connected_id = "post:#{self.post_id}"
      end
    end
  end

  def users_to_send_notifications_to
    user_ids = self.post.comments.pluck(:user_id) + [self.post.user_id]
    user_ids.uniq
  end
end
