class Skill < ApplicationRecord
  belongs_to :user

  validates :name, presence: true, uniqueness: { scope: :user_id }
  validates :kind_of, inclusion: %w(already_know want_to_learn currently_learning)

  scope :already_know, -> { where(kind_of: :already_know) }
  scope :want_to_learn, -> { where(kind_of: :want_to_learn) }
  scope :currently_learning, -> { where(kind_of: :currently_learning) }
end
