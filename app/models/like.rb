class Like < ApplicationRecord
  belongs_to :post
  belongs_to :user

  after_create :send_notification

  private

  def send_notification
    return if self.post.user == self.user

    link = if self.post.classroom_id.present?
             Rails.application.routes.url_helpers.classroom_post_path(self.post, classroom_id: self.post.classroom_id)
           else
             Rails.application.routes.url_helpers.post_path(self.post)
           end

    Notification.create do |notif|
      notif.user_id      = self.post.user_id
      notif.message      = "#{self.user.name} has liked your post."
      notif.link         = link
      notif.connected_id = "post:#{self.post_id}"
    end
  end
end
