class User < ApplicationRecord
  has_friendship
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :omniauthable, omniauth_providers: [:google_oauth2]

  has_many :skills
  has_many :user_classrooms
  has_many :classrooms, through: :user_classrooms
  has_many :likes
  has_many :comments
  has_many :notifications
  has_many :posts

  validates :email, uniqueness: true
  validates :name, allow_nil: true, length: { minimum: 2 }
  validates :age, allow_nil: true, inclusion: 1..120
  validates :username, allow_nil: true, format: { with: /\A(?=.*[a-z])[a-z\d]+\Z/i, message: 'should be alphanumeric' }, uniqueness: true

  def to_param
    username
  end

  def unread_notifications
    notifications.where(is_read: false)
  end

  def incomplete_credentials?
    name.nil? || age.nil? || username.nil? || encrypted_password.blank?
  end

  def self.from_omniauth(auth)
    # Either create a User record or update it based on the provider (Google) and the UID
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.token = auth.credentials.token
      user.expires = auth.credentials.expires
      user.expires_at = auth.credentials.expires_at
      user.refresh_token = auth.credentials.refresh_token
    end
  end

  def has_class?(classroom)
    classrooms.include?(classroom)
  end
end
