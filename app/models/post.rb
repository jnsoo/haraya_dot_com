class Post < ApplicationRecord
  belongs_to :user
  belongs_to :classroom, optional: true

  has_many :likes
  has_many :comments

  validates :body, presence: true
end
