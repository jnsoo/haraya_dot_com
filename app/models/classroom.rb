class Classroom < ApplicationRecord
  belongs_to :user

  has_many :user_classrooms
  has_many :users, through: :user_classrooms
  has_many :posts

  validates_presence_of :name
  validates_presence_of :description
  validates :group_id, uniqueness: true, presence: true, format: { with: /[\w \.\-@]+/, message: 'should be alphanumeric' }

  def to_param
    group_id
  end
end
