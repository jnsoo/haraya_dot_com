class AddConnectedIdToNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :notifications, :connected_id, :string
  end
end
