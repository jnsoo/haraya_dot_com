class RenameTypeColumn < ActiveRecord::Migration[5.2]
  def change
    rename_column :skills, :type, :kind_of
  end
end
