class AddUsernameToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :username, :string

    User.all.each do |user|
      user.update(username: "user#{user.id}")
    end
  end
end
