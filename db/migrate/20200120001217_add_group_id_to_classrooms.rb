class AddGroupIdToClassrooms < ActiveRecord::Migration[5.2]
  def change
    add_column :classrooms, :group_id, :string

    Classroom.all.each do |classroom|
      classroom.update(group_id: "#{classroom.name.gsub(' ', '_')}_#{classroom.id}")
    end
  end
end
