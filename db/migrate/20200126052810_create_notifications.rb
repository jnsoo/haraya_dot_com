class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.text :message
      t.references :user, foreign_key: true
      t.boolean :is_read, default: false

      t.timestamps
    end
  end
end
