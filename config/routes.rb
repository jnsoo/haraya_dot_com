Rails.application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", registrations: 'registrations' }

  scope module: :users, path: 'users' do
    resource :registration, only: [:show, :update]
    resource :change_passwords, only: [:edit, :update]
  end

  scope module: :classrooms, path: 'classrooms' do
    resource  :join,       only: [:create, :destroy]
    resources :my_classes, only: :index
  end

  resources :profiles,    only: [:index]
  resources :skills,      only: [:create, :destroy]
  resource  :users,       only: [:edit, :update]
  resources :users,       only: [:index, :show]
  resources :friendships, only: [:create, :destroy, :update]
  resources :posts,       only: [:new, :create, :edit, :update, :destroy, :show]
  resources :likes,       only: [:create]
  resources :comments,    only: [:create]

  resources :classrooms, only: [:index, :new, :create, :show, :edit, :update] do
    scope module: :classrooms do
      resources :posts, only: [:new, :create, :edit, :update, :destroy, :show]
    end
  end

  root to: 'home#index'
end
